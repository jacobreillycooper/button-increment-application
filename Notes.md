# React learning

Looking at Modern JavaScript. 
jscomplete.com/react-cfp - common errors faced in React.

## Notes

1. Components - simply... functions
Input: props, state | Output is the UI


## Code Snippets

```JSX
function App() {
    const [counter, setCounter] = useState(0);

    return (
        <button onClick={() => setCounter(counter+1)}>{counter}</button>
    )
}
/**
 * A challenge that was given: 
 * Make the counter start at 5 and then double.
 * Solution is simply useState(5) & setCounter(counter*2)
 * 
 * Nothing too troubling, the introduction made sense. Using javascript knowledge. Notes made inside of the project.
```
## Props


