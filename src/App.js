import React, { useState } from 'react';
import Button from './Button/Button'
import './App.css';

function Display(props) {
  return (
    <div>
      {props.message}
    </div>
  )
}


function App() {
  const [counter, setCounter] = useState(0);
  const incrementCounter = (incrementValue) => setCounter(counter + incrementValue);
  return (
    // useState()
    // returing two items. State object (getter)
    // updater function (setter)
    <div>
      <Button onClickFunction={incrementCounter} increment={1} />
      <Button onClickFunction={incrementCounter} increment={5} />
      <Button onClickFunction={incrementCounter} increment={10} />
      <Button onClickFunction={incrementCounter} increment={100} />
      <Display message={counter} />
    </div>
    // case sensitive - components should have capital letter.
  )

}

export default App;
