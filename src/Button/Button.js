import React from 'react';

function Button(props) {
    // initial state value (in here)

    // const handleClick = 
    return (

        // <div>Hello React!</div>
        // React.createElement('div', null, 'Hello React')
        // jsx 

        <button onClick={() => props.onClickFunction(props.increment)}>
            +{props.increment}
        </button>

    )
}

export default Button;